# Introduction

Hi, My Name is **Febriansyah**. I am software engineer from Bandung, Indonesia. I have 4 years+ experience in this field. I created this Repo to show my projects so you can see my code style. my code skill and my learning progress.

# My Main Programming Languages
- Laravel (4 Years +)
- Golang (2 Years +)
- NodeJS (1 Year +)

# Tech Stack That I've Ever Used
**Frontend:** 
- Vuejs
- Hugo

**Backend:** 
- ExpressJS
- AdonisJS
- NestJs
- Codeigniter

**Devops:** 
- Digital Ocean 
- IDCloudHost 
- Docker
- Gitlab CI/CD
- Linux

**Other:**
- Git
- Trello
- Jira
- Slack
- Mattermost
- Discord


# Projects

#### API Product
- Type : Backend
- Description : I created this application for my learning. and try to implement new things that I've Learned.
- Tech Stack: Laravel, Laravel Unit Test, Laravel Requests, Laravel Resources. Laravel Logging, Laravel TryCatch, Laravel storage, Laravel Softdeletes, DB Transaction, and Docker
- Url : https://gitlab.com/belajar1380705/aplikasi-product/backend

#### App Schedular

- Type : Fullstack Laravel with Laravel Blade
- Description : I created this project for the recruitment process. I apply for Fullstack Developer role with Laravel, Jquery Stacks. if you want to know how do I code with Jquery and Laravel. I sugguest you to open this project.
- Tech Stack: Laravel, Laravel requests, Laravel Resources, Laravel Logging, Laravel TryCatch, DB Transaction, Jquery, Ajax
- URL : https://gitlab.com/belajar1380705/aplikasi-schedular/laravel
